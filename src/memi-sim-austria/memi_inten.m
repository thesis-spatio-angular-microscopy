% memi_inten.m
function inten=memi_inten(px,py,mpx,mpy,mr,lpx,lpy,lr,h_asf,filtersize)
Ex=squeeze(h_asf(:,:,:,0));
Ey=squeeze(h_asf(:,:,:,1));
Ez=squeeze(h_asf(:,:,:,2));

%% Lightsource
sx=size(h_asf,1);sy=size(h_asf,2);sz=size(h_asf,3);
%px=13;py=13;
%mpx=5;mpy=0;
%lpx=7;lpy=5;
tun=newim(sx,sy);
tun(px,py)=1;
ft_tun=ft(tun);
%% MMA
t=sqrt((xx(sx,sy)-mpx).^2+(yy(sx,sy)-mpy).^2)<mr;
if filtersize>0
    t=gaussf(t,filtersize);
end
lcos_ill=ift(ft_tun.*t);
%% LCOS
L=sqrt((xx(sx,sy)-lpx).^2+(yy(sx,sy)-lpy).^2)<lr;
lcos_out=lcos_ill.*L;
%% Apply ASF to Amplitude after LCOS
lcos_3D=newim(sx,sy,sz,'complex');
lcos_3D(:,:,floor(sz/2))=lcos_out;
Esx=convolve(lcos_3D,Ex);
Esy=convolve(lcos_3D,Ey);
Esz=convolve(lcos_3D,Ez);
inten=real(Esx*conj(Esx)+Esy*conj(Esy)+Esz*conj(Esz));
