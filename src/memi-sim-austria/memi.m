% memi.m
%% Lightsource
sx=30;sy=30;sz=30;
px=17;py=17;
mpx=5;mpy=0;
lpx=7;lpy=5;
lr=10;mr=3;
%% ASF
h_asf=kSimPSF( {'na',1.38;'ri',1.52;'sX',sx;'sY',sy;'sZ',sz;'scaleX',80;'scaleY',80;'scaleZ',160;'computeASF',1;'circPol',0;'scalarTheory',0})
h_asf=squeeze(h_asf);

%% incoherent
incor=0;
for px=0:29
    for py=0:29
        inten=memi_inten(px,py,mpx,mpy,mr,lpx,lpy,lr,h_asf,1);
        incor=incor+inten;
        fprintf('px: %d, py: %d\n',px,py);
    end
end
